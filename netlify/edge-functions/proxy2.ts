export default async function proxy(request: Request) {
  if (request.method !== "GET") {
    return new Response("Method not allowed", { status: 405 });
  }

  const url = new URL(request.url);

  // Get server ip address
  const server = await fetch("https://api.ipify.org?format=json");
  const serverIp = await server.json();
  const serverIpAddr = serverIp.ip;

  return new Response(
    `Hello from proxy for ${url}, the ip address of the server is ${serverIpAddr}`
  );
}

export const config = { path: "/proxy2" };
