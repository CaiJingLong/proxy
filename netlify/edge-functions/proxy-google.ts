export default async function proxy(request: Request) {
  const urlToProxy = 'https://google.com'
  const response = await fetch(urlToProxy);
  return response;
}

export const config = { path: "/proxy-google" };
