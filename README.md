# Proxy

代理服务器，用于将请求转发到不同的服务器上。

host 代表你部署的域名

目前只支持 `GET` 请求，且不支持 cookies

## Netlify

部署到 Netlify，并利用 Netlify 的边缘函数能力，将请求转发到不同的服务器上。

其中主要对应 `netlify/edge-functions` 目录下的文件。

当前 proxy 地址为 `{host}/proxy`

## Vercel

部署到 Vercel，并利用边缘 Vercel 的边缘函数能力，将请求转发到不同的服务器上。

当前 proxy 地址为 `{host}/api/proxy`
