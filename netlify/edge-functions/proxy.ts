export default async function proxy(request: Request) {
  if (request.method !== "GET") {
    return new Response("Method not allowed", { status: 405 });
  }

  const url = new URL(request.url);

  const queryParmas = url.searchParams;
  const urlToProxy = queryParmas.get("url");

  if (!urlToProxy) {
    return new Response("Missing url query param", { status: 400 });
  }

  console.log(`Proxying request to ${urlToProxy}`)
  const targetURL = new URL(urlToProxy);
  const response = await fetch(targetURL);
  return response;
}

export const config = { path: "/proxy" };
